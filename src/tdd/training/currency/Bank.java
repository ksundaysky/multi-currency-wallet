package tdd.training.currency;

import java.util.Hashtable;

class Bank {
    private Hashtable rates = new Hashtable();

    Money reduce(Expression source, String to){
        return source.reduce(this,to);
    }

    int rate(String from, String to){
        if(to.equals(from)) return 1;
        return (Integer)this.rates.get(new Pair(from,to));
    }

    void addRate(String from, String to, int rate){
        this.rates.put(new Pair(from,to),rate);
    }

}
